#!/bin/bash
if [ ! -d /www/sifo ]; then
	echo "SIFO is not present on /www/sifo. We're cloning it from GitHub."
	git clone --recursive git://github.com/alombarte/SIFO.git /www/sifo
else
	echo "SIFO is already present on /www/sifo."
fi

exit